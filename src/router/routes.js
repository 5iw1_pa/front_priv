import roles from "../helpers/roles.enum";
import store from '@/state/store'

export default [
    {
        path: '/logout',
        name: 'logout',
        meta: {
            authRequired: true,
            beforeResolve(routeTo, routeFrom, next) { 
                if (process.env.VUE_APP_DEFAULT_AUTH === "firebase") { 
                    store.dispatch('auth/logOut') 
                } else { 
                    store.dispatch('authfack/logout') 
                } 
                const authRequiredOnPreviousRoute = routeFrom.matched.some( 
                    (route) => route.push('/login') 
                ) 
                // Navigate back to previous page, or home as a fallback 
                next(authRequiredOnPreviousRoute ? { name: 'home' } : { ...routeFrom }) 
            }, 

        },
    },
    {
        path: '/authent',
        name: 'authent',
        meta: {
            authRequired: false,
        },
        component: () => import('../views/pages/account/authent')
    },
    {
        path: '/blocked_account',
        name: 'blockedAccount',
        meta: {
            authRequired: false,
        },
        component: () => import('../views/pages/account/blockedAccount')
    },
    {
        path: '/change-email',
        name: 'change-email',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/account/changeEmail')
    },
    {
        path: '/profil',
        name: 'Display profil',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/account/profilDisplay')
    },
    {
        path: '/user/:id',
        name: 'ProfilCard',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/account/profilCard')
    },
    {
        path: "/users",
        name: "Users",
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import("../views/pages/users/userList")
    },
    {
        path: '/editprofil',
        name: 'edit profil',
        meta: {
            authRequired: false,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/account/editprofil')
    },
    {
        path: '/editprofilPro',
        name: 'edit profil Pro',
        meta: {
            authRequired: false,
            roles: [roles.ROLE_USER, roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/account/editProfilPro')
    },
    {
        path: '/',
        name: 'home',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/dashboard/index')
    },
    {
        path: '/ticketslist',
        name: 'Tickets List',
        meta: { 
           authRequired: true,
           roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/support/ticketsList')
    },
    {
        path: '/createTicket',
        name: 'Tickets create',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/support/createTicket')
    },
    {
        path: '/ticket/:id',
        name: 'ticket_display',
        meta: {
            authRequired: false,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/support/displayTicket')
    },
    {
        path: '/createMission',
        name: 'Form mission create',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/missions/createMission')
    },
    {
        path: '/missions',
        name: 'Missions',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/missions/missionListRole')
    },
    {
        path: '/mission/:id',
        name: 'Mission detail',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/missions/oneMissionDisplay')
    },
    {
        path: '/edit/mission/:id',
        name: 'edit_mission',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/missions/editMission')
    },
    {
        path: '/enterprise/missions',
        name: 'my_mission_enterprise',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/missions/myMissionEnterprise')
    },
    {
        path: '/freelance/missions/candidate',
        name: 'my_mission_freelance_candidate',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/missions/myMissionFreelanceCandidate')
    },
    {
        path: '/freelance/missions/validate',
        name: 'my_mission_freelance_validate',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/missions/myMissionFreelanceAccepted')
    },
    {
        path: '/subscriptionsList',
        name: 'Subscription List',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/subscriptions/subscriptionsList')
    },
    {
        path: '/admin/users',
        name: 'admin_user_list',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/admin/usersList')
    },
    {
        path: '/admin/tickets',
        name: 'admin_tickets_list',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/admin/ticketsList')
    },
    {
        path: '/admin/user/:id',
        name: 'admin_profile_user',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/admin/userProfile')
    },
    {
        path: '/admin/premium-offer',
        name: 'admin_premium_offer',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/admin/premiumOffer')
    },
    {
        path: '/enterprise/admin',
        name: 'enterprise_admin',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/enterprise/enterpriseAdmin')
    },
    {
        path: '/enterprise/admin/add',
        name: 'enterprise_admin_add_user',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN]
        },
        component: () => import('../views/pages/enterprise/addUserEnterprise')
    },
    {
        path: '/admin/edit/user/:id',
        name: 'admin_edit_user',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/admin/userEdit')
    },
    {
        path: '/subscription/:id',
        name: 'Subscription Display',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/subscriptions/oneSubscription')
    },
    {
        path: '/paymentList',
        name: 'payment list',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/payment/PaymentList')
    },
    {
        path: '/payment/:id',
        name: 'user_payment_show',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/payment/showPayment')
    },
    {
        path: '/admin/payments',
        name: 'payment list admin',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/PaymentListAdmin/PaymentList')
    },
    {
        path: '/admin/payment/:id',
        name: 'admin_payment_show',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/PaymentListAdmin/showPayment')
    },
    {
        path: '/admin/refunds',
        name: 'refund list admin',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/PaymentListAdmin/refundList')
    },
    {
        path: '/admin/refund/:id',
        name: 'admin_refund_show',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/PaymentListAdmin/showRefund')
    },
    {
        path: '/admin/not-succeded',
        name: 'not succeded payment admin',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ADMIN]
        },
        component: () => import('../views/pages/PaymentListAdmin/notSuccededPayment')
    },
    {
        path: '/premium',
        name: 'Premium Page',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/premium/premiumPage')
    },
    {
        path: '/premium/success',
        name: "Premium Sucess",
        meta: {
            authRequired: true,
            roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE]
        },
        component: () => import('../views/pages/premium/premiumSuccess')
    },
    {
        path: '/maintenance',
        name: 'Maintenance',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/maintenance/index')
    },
    {
        path: '/error-404',
        name: 'Error-404',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/errorPage/error-404')
    },
    {
        path: '/error-500',
        name: 'Error-500',
        meta: {
            authRequired: true,
            roles: [roles.ROLE_USER]
        },
        component: () => import('../views/pages/errorPage/error-500')
    }
]
