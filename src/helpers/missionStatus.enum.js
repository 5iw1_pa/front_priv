const missionStatus = Object.freeze({
  COMING: 0,
  IN_PROGRESS: 1,
  COMPLETED: 2,
})

export default missionStatus;
