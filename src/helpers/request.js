// import * as _ from 'lodash';
let axios = require('axios');
const request = function () {};

request.create = function (uri, endpoint) {
  let _endPoint = process.env.VUE_APP_API_URL;

  return {
    uri: endpoint === "public" ? uri : _endPoint + uri,

    expiredJWTToken: function (res) {
      if (res.response.data.message === "Expired JWT Token" || res.response.data.message === "Invalid credentials.") {
        window.location.href = "/logout";
      }
    },

    createHeaders: function (headers) {
      const token = localStorage.getItem('token');
      let header = {
        'content-type': 'application/json',
      };
      if (token) {
        header.Authorization = `Bearer ${token}`;
      }
      header = Object.assign(header, headers);
      return header;
    },

    get: async function (params, responseType, headers) {
      try {
        return await axios({
          method: 'GET',
          url: this.uri,
          params: params,
          headers: this.createHeaders(headers),
          catch: (err) => {console.log(err)},
          responseType: responseType ? responseType : 'json',
        });
      } catch(e) { 
        this.expiredJWTToken(e);
        return e;
      }      
    },

    post: async function (payload, headers, responseType) {
      try {
        return await axios({
          method: 'POST',
          url: this.uri,
          headers: this.createHeaders(headers),
          data: JSON.stringify(payload),
          processData: false,
          responseType: responseType ? responseType : 'json',
          catch: (err) => {console.log(err)},
        });
      } catch (e) {
        this.expiredJWTToken(e);
        return e;
      }
    },

    put: async function (payload, headers) {
      try {
        return await axios({
          method: 'PUT',
          url: this.uri,
          headers: this.createHeaders(headers),
          data: JSON.stringify(payload),
          processData: false,
          catch: (err) => {console.log(err);},
        });
      } catch (e) {
        this.expiredJWTToken(e);
        return e;
      } 
    },

    delete: async function () {
      try {
       return await axios({
          method: 'DELETE',
          url: this.uri,
          headers: this.createHeaders(),
          catch: (err) => {console.log(err);},
        });
      } catch (e) {
        this.expiredJWTToken(e);
        return e;
      }
    },

    patch: async function () {
      try {
        return await axios({
          method: 'PATCH',
          url: this.uri,
          headers: this.createHeaders(),
          catch: (err) => {console.log(err);},
        });
      } catch (e) {
        this.expiredJWTToken(e);
        return e;
      }
    }
  };
};

export default request;
