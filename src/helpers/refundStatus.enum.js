const refundStatus = Object.freeze({
  WAITING: 0,
  REFUNDED: 1,
  REFUSED: 2,
  CANCEL: 3,
})

export default refundStatus;
