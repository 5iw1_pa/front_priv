export const menuItems = [
    {
        id: 1,
        label: 'menuitems.dashboard.text',
        icon: 'ri-dashboard-line',
        link: '/'
    },
    {
        id: 2,
        label: 'menuitems.uielements.text',
        icon: 'ri-pencil-ruler-2-line',
        subItems: [
            {
                id: 3,
                label: 'menuitems.uielements.list.alerts',
                link: '/ui/alerts'
            },
            {
                id: 4,
                label: 'menuitems.uielements.list.buttons',
                link: '/ui/buttons'
            },
            {
                id: 5,
                label: 'menuitems.uielements.list.cards',
                link: '/ui/cards'
            },
            {
                id: 6,
                label: 'menuitems.uielements.list.carousel',
                link: '/ui/carousel'
            },
            {
                id: 7,
                label: 'menuitems.uielements.list.dropdowns',
                link: '/ui/dropdowns'
            },
            {
                id: 8,
                label: 'menuitems.uielements.list.grid',
                link: '/ui/grid'
            },
            {
                id: 9,
                label: 'menuitems.uielements.list.images',
                link: '/ui/images'
            },
            {
                id: 10,
                label: 'menuitems.uielements.list.lightbox',
                link: '/ui/lightbox'
            },
            {
                id: 11,
                label: 'menuitems.uielements.list.modals',
                link: '/ui/modals'
            },
            {
                id: 16,
                label: 'menuitems.uielements.list.tabs',
                link: '/ui/tabs-accordion'
            },
            {
                id: 17,
                label: 'menuitems.uielements.list.typography',
                link: '/ui/typography'
            },
            {
                id: 18,
                label: 'menuitems.uielements.list.video',
                link: '/ui/video'
            },
            {
                id: 19,
                label: 'menuitems.uielements.list.general',
                link: '/ui/general'
            },
            {
                id: 20,
                label: 'menuitems.uielements.list.rating',
                link: '/ui/rating'
            },
            {
                id: 21,
                label: 'menuitems.uielements.list.notifications',
                link: '/ui/notification'
            }
        ]
    },
    {
        id: 37,
        label: 'menuitems.components.text',
        icon: 'ri-stack-line',
        subItems: [
            {
                id: 38,
                label: 'menuitems.forms.text',
                subItems: [
                    {
                        id: 39,
                        label: 'menuitems.forms.list.elements',
                        link: '/form/elements'
                    },
                    {
                        id: 40,
                        label: 'menuitems.forms.list.validation',
                        link: '/form/validation'
                    },
                    {
                        id: 41,
                        label: 'menuitems.forms.list.advanced',
                        link: '/form/advanced'
                    },
                    {
                        id: 42,
                        label: 'menuitems.forms.list.editor',
                        link: '/form/editor'
                    },
                    {
                        id: 43,
                        label: 'menuitems.forms.list.fileupload',
                        link: '/form/uploads'
                    },
                    {
                        id: 44,
                        label: 'menuitems.forms.list.wizard',
                        link: '/form/wizard'
                    },
                    {
                        id: 45,
                        label: 'menuitems.forms.list.mask',
                        link: '/form/mask'
                    }
                ]
            },
            {
                id: 46,
                label: 'menuitems.tables.text',
                subItems: [
                    {
                        id: 47,
                        label: 'menuitems.tables.list.basic',
                        link: '/tables/basic'
                    },
                    {
                        id: 48,
                        label: 'menuitems.tables.list.advanced',
                        link: '/tables/advanced'
                    },
                ]
            },
        ]
    },
    {
        id: 61,
        label: 'menuitems.pages.text',
        icon: 'ri-file-copy-2-line',
        subItems: [
            {
                id: 67,
                label: 'menuitems.users.text',
                subItems: [
                    {
                        id: 69,
                        label: 'menuitems.utility.list.maintenance',
                        link: '/pages/maintenance'
                    },
                    {
                        id: 71,
                        label: 'menuitems.utility.list.timeline',
                        link: '/pages/timeline'
                    },
                    {
                        id: 72,
                        label: 'menuitems.utility.list.faqs',
                        link: '/pages/faqs'
                    },
                    {
                        id: 73,
                        label: 'menuitems.utility.list.pricing',
                        link: '/pages/pricing'
                    },
                    {
                        id: 74,
                        label: 'menuitems.utility.list.error404',
                        link: '/pages/error-404'
                    },
                    {
                        id: 75,
                        label: 'menuitems.utility.list.error500',
                        link: '/pages/error-500'
                    }
                ]
            }
        ]
    }
]