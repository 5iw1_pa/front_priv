import roles from "../helpers/roles.enum";
import jwt from "jsonwebtoken";

const token = localStorage.getItem('token');
const decodedToken = jwt.decode(token);

const displayItem = function (roles) {
  return decodedToken.roles.some(r => roles.includes(r));
}

const menuItems = [
  {
    id: 1,
    label: "menuitems.menu.text",
    isTitle: true,
    roles: [roles.ROLE_USER]
  },
  {
    id: 2,
    label: "menuitems.dashboard.text",
    icon: "ri-dashboard-line",
    link: "/",
    roles: [roles.ROLE_USER]
  },
  {
    id: 3,
    isLayout: true,
    roles: [roles.ROLE_USER]
  },
  {
    id: 4,
    label: "menuitems.pages.text",
    isTitle: true,
    roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE]
  },
  {
    id: 5,
    label: "menuitems.users.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
    subItems: [
      {
        id: 6,
        label: "menuitems.users.list.listusers",
        link: "/users",
        roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
      }
    ],
  },
  {
    id: 7,
    label: "menuitems.mission.text",
    isTitle: true,
    roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
  },
  {
    id: 8,
    label: "menuitems.mission.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
    subItems: [
      {
        id: 9,
        label: "menuitems.mission.list.listmission",
        link: "/missions",
        roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
      },
      {
        id: 10,
        label: "menuitems.mission.list.createmission",
        link: "/createMission",
        roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN],
      },
      {
        id: 1000,
        label: "menuitems.mission.list.myMissionEnterprise",
        link: "/enterprise/missions",
        roles: [roles.ROLE_ENTERPRISE, roles.ROLE_ENTERPRISE_ADMIN],
      },
      {
        id: 1001,
        label: "menuitems.mission.list.myMissionFreelanceCandidate",
        link: "/freelance/missions/candidate",
        roles: [roles.ROLE_FREELANCE],
      },
      {
        id: 1002,
        label: "menuitems.mission.list.myMissionFreelanceValidate",
        link: "/freelance/missions/validate",
        roles: [roles.ROLE_FREELANCE],
      }
    ],
  },
  {
    id: 11,
    label: "menuitems.admin.text",
    isTitle: true,
    roles: [roles.ROLE_ADMIN],
  },
  {
    id: 12,
    label: "menuitems.admin.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_ADMIN],
    subItems: [
      {
        id: 13,
        label: "menuitems.admin.list.listsubsciption",
        link: "/subscriptionsList",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 130,
        label: "menuitems.admin.list.users",
        link: "/admin/users",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 24,
        label: "menuitems.admin.list.paymentslist",
        link: "/admin/payments",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 25,
        label: "menuitems.admin.list.refundlist",
        link: "/admin/refunds",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 25,
        label: "menuitems.admin.list.notsuccededpayment",
        link: "/admin/not-succeded",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 25,
        label: "menuitems.admin.list.tickets",
        link: "/admin/tickets",
        roles: [roles.ROLE_ADMIN],
      },
      {
        id: 25,
        label: "menuitems.admin.list.premiumOffer",
        link: "/admin/premium-offer",
        roles: [roles.ROLE_ADMIN],
      },
    ],
  },
  {
    id: 111,
    label: "menuitems.enterprise_admin.text",
    roles: [roles.ROLE_ENTERPRISE_ADMIN],
    isTitle: true,
  },
  {
    id: 112,
    label: "menuitems.enterprise_admin.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_ENTERPRISE_ADMIN],
    subItems: [
      {
        id: 113,
        label: "menuitems.enterprise_admin.list",
        link: "/enterprise/admin",
        roles: [roles.ROLE_ENTERPRISE_ADMIN],
      },
    ],
  },
  {
    id: 14,
    label: "menuitems.support.text",
    isTitle: true,
    roles: [roles.ROLE_USER],
  },
  {
    id: 15,
    label: "menuitems.support.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_USER],
    subItems: [
      {
        id: 16,
        label: "menuitems.support.list.listticket",
        link: "/ticketslist",
        roles: [roles.ROLE_USER],
      },
      {
        id: 17,
        label: "menuitems.support.list.create",
        link: "/createTicket",
        roles: [roles.ROLE_USER],
      }
    ],
  },
  {
    id: 18,
    label: "menuitems.payment.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_ADMIN, roles.ROLE_FREELANCE],
    subItems: [
      {
        id: 19,
        label: "menuitems.payment.list.listpay",
        link: "/paymentList",
        roles: [roles.ROLE_ENTERPRISE_ADMIN, roles.ROLE_FREELANCE],
      }
    ],
  },
  {
    id: 21,
    label: "menuitems.premium.text",
    isTitle: true,
    roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE_ADMIN],
  },
  {
    id: 22,
    label: "menuitems.premium.text",
    icon: "ri-profile-line",
    roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE_ADMIN],
    subItems: [
      {
        id: 23,
        label: "menuitems.premium.list.info",
        link: "/premium",
        roles: [roles.ROLE_FREELANCE, roles.ROLE_ENTERPRISE_ADMIN],
      }
    ],
  },
];

const menu = menuItems.filter(m => {
  if (m.subItems) {
    m.subItems = m.subItems.filter(subItem => displayItem(subItem.roles))
  }
  return displayItem(m.roles);
});

export default menu;
